#!/bin/sh
#
# Cloud Hook: post-db-copy
#
# The post-db-copy hook is run whenever you use the Workflow page to copy a
# database from one environment to another. See ../README.md for
# details.
#
# Usage: post-db-copy site target-env db-name source-env

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"

ALIAS=$site.$target_env

echo "\nDisabling page caching ..."
drush @$ALIAS vset cache 0
drush @$ALIAS vset block_cache 0

echo "\nDisabling bandwidth optimization ..."
drush @$ALIAS vset page_compression 0
drush @$ALIAS vset preprocess_css 0
drush @$ALIAS vset preprocess_js 0

echo
