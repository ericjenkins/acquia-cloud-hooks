#!/bin/sh
#
# Cloud Hook: update-db
#
# Run drush updatedb in the target environment. This script works as
# any Cloud hook.

site="$1"
target_env="$2"

echo "\nRunning update.php on $site.$target_env ..."
drush @$site.$target_env updatedb --yes

echo
