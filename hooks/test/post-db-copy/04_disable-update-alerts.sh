#!/bin/sh
#
# Cloud Hook: post-db-copy
#
# The post-db-copy hook is run whenever you use the Workflow page to copy a
# database from one environment to another. See ../README.md for
# details.
#
# Usage: post-db-copy site target-env db-name source-env

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"

ALIAS=$site.$target_env

echo "\nEnabling update module & clearing notification email addresses ..."
drush @$ALIAS pm-enable -y update
drush @$ALIAS vdel -y update_notify_emails

echo
