# What are Cloud Hooks? #
* Source: [Github](https://github.com/acquia/cloud-hooks)

Cloud Hooks is a feature of Acquia Cloud, the Drupal cloud hosting platform. For more information, see [here](https://www.acquia.com/products-services/acquia-dev-cloud).

The Acquia Cloud Workflow page automates the most common tasks involved in developing a Drupal site: deploying code from a version control system, and migrating code, databases, and files across your Development, Staging, and Production environments. Cloud Hooks allow you to automate other tasks as part of these migrations.

A Cloud Hook is simply a script in your code repository that Acquia Cloud executes on your behalf when a triggering action occurs. Examples of tasks that you can automate with Cloud Hooks include:

* Perform Drupal database updates each time new code is deployed.
* "Scrub" your Production database when it is copied to Dev or Staging by removing customer emails or disabling production-only modules.
* Run your test suite or a site performance test each time new code is deployed.

# Sample Implementation #
This sample file structure of Acquia hooks was adapted from a real-life example:
```
repository
   |
   |--docroot
   |
   |--hooks (contains all the Acquia hook files)
        |
        |--common (hooks that execute for all 3 environments - Dev/Stage/Prod)
        |   |
        |   |--post-code-deploy (hooks that execute after checking out a code branch in the Acquia workflow portal)
        |           |
        |           |--01_slack.sh                 (Notify the #acquia channel in Slack)
        |           |--02_backup-db.sh             (Execute an Acquia database backup)
        |           |--03_update-db.sh             (Run update.php)
        |           |--04_ap-purge.sh              (Run Acquia Purge on <front>)
        |
        |--dev (hooks that execute only for Acquia Dev environment)
        |   |
        |   |--post-db-copy (hooks that execute after importing a database using the Acquia workflow portal)
        |           |
        |           |--01_update-db.sh             (Run update.php)
        |           |--02_ap-purge                 (Run Acquia Purge on <front>)
        |           |--03_disable-caching.sh       (Turn off js/css/page compression)
        |           |--04_disable-analytics.sh     (Turn off Google Analytics modules)
        |           |--05_disable-update-alerts.sh (Disable update notification emails)
        |           |--06_update-site-email.sh     (Change site email to example@mysite.com)
        |           |--07_update-webform-emails.sh (Change webform notification emails to example@mysite.com)
        |
        |--test (hooks that execute only for Acquia Stage environment)
            |
            |--post-db-copy
                    |
                    |--01_update-db.sh             (Run update.php)
                    |--02_ap-purge.sh              (Run Acquia Purge on <front>)
                    |--03_disable-analytics.sh     (Turn off Google Analytics modules)
                    |--04_disable-update-alerts.sh (Disable update notification emails)
```